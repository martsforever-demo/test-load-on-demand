import Vue from 'vue'
import App from './App.vue'
import VueCompositionApi from '@vue/composition-api'

Vue.use(VueCompositionApi)


/*---------------------------------------全局引入-------------------------------------------*/
import PLAIN from 'plain-ui'
import 'plain-ui/dist/plain-ui.css'

Vue.use(PLAIN)

/*---------------------------------------按需加载-------------------------------------------*/
/*import {button} from 'plain-ui'

Vue.use(button)*/


Vue.config.productionTip = false
new Vue({
    render: h => h(App),
}).$mount('#app')
